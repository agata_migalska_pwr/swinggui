package pl.edu.pwr.lab4.wt13;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SrodkowyPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6831973288572688871L;
	
	public SrodkowyPanel() {
		this.setLayout(new GridLayout(4, 1));
		
		JTextField textField = new JTextField();
		textField.setText("Jestem polem tekstowym");
		this.add(textField);
		JTextArea textArea = new JTextArea();
		textArea.setText("Jestem obszarem tekstowym");
		this.add(textArea);
	}

}
