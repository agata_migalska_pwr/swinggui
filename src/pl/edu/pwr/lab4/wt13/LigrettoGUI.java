package pl.edu.pwr.lab4.wt13;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class LigrettoGUI {
	
	public static void main(String[] args) {
		LigrettoGUI gui = new LigrettoGUI();
		gui.stworzGui();
	}

	public void stworzGui() {
		JFrame frame = new JFrame();
		JButton button = new JButton("Guzik");
		button.setEnabled(false);
		ActionListener buttonActionListener = new ButtonActionListener();
		button.addActionListener(buttonActionListener);
		
		frame.getContentPane().add(BorderLayout.NORTH, button);
		
		SrodkowyPanel srodkowyPanel = new SrodkowyPanel();
		frame.getContentPane().add(BorderLayout.CENTER, srodkowyPanel);
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GREEN);
		frame.getContentPane().add(BorderLayout.EAST, panel);
		
		panel.add(new JButton("test"));
		panel.add(new JButton("test2"));
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
//		frame.repaint();
		frame.setTitle("Ligretto");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		frame.setVisible(true);
	}

}
