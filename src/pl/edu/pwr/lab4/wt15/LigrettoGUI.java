package pl.edu.pwr.lab4.wt15;

import javax.swing.JFrame;

public class LigrettoGUI {

	public static void main(String[] args) {
		LigrettoGUI gui = new LigrettoGUI();
		gui.stworzGui();
	}

	private void stworzGui() {
		JFrame frame = new JFrame();
		frame.setSize(500,500);
		frame.setTitle("Ligretto");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MojPanel mainPanel = new MojPanel();
		frame.add(mainPanel);
		
		frame.setVisible(true);
		
	}

}
