package pl.edu.pwr.lab4.sr15;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PanelAdministracyjny extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8468386613927124735L;
	private JTextField nameField;
	private JPasswordField pinField;
	private JButton submitButton;

	public PanelAdministracyjny() {
		stworzZawartoscPanelu();
	}

	private void stworzZawartoscPanelu() {
		
		JLabel nameLabel = new JLabel("Imię:");
		this.add(nameLabel);
		
		nameField = new JTextField();
		this.add(nameField);
		
		JLabel pinLabel = new JLabel("PIN:");
		this.add(pinLabel);
		
		pinField = new JPasswordField();
		this.add(pinField);
		
		this.add(new JLabel());
		
		submitButton = new JButton("Wyślij");
		this.add(submitButton);
		
		submitButton.addActionListener(this);
		
		this.setLayout(new GridLayout(3, 2, 5, 5));
		
		this.setBackground(Color.WHITE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String name = nameField.getText();
		char[] pin = pinField.getPassword();
		System.out.print(name + " ");
		for (char pinElement : pin) {
			System.out.print(pinElement);
		}
		submitButton.setText("Wysłano");		
	}
	
}
