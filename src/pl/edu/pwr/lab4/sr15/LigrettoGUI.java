package pl.edu.pwr.lab4.sr15;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LigrettoGUI {
	
	public static void main(String[] args) {
		LigrettoGUI gui = new LigrettoGUI();
		gui.stworzGui();
	}

	private void stworzGui() {
		JFrame frame = new JFrame();
		frame.setTitle("Formularz Dnia");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(500, 500));
		
		JPanel panel = stworzPanelGlowny(); 
		frame.getContentPane().add(panel);
		
		JButton button = new JButton("Kliknij");
		frame.getContentPane().add(BorderLayout.NORTH, button);
		
		JTextField textField = new JTextField();
		frame.getContentPane().add(BorderLayout.SOUTH, textField);
		
		frame.setVisible(true);
	}

	private JPanel stworzPanelGlowny() {
		return new PanelAdministracyjny();
	}

}
