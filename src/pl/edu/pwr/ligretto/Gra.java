package pl.edu.pwr.ligretto;

import pl.edu.pwr.ligretto.gui.LigrettoGUI;
import pl.edu.pwr.ligretto.model.Talia;

/**
 * 
 * Główna klasa gry w Ligretto. Zakłada się, że w grę gra 4 graczy, każdy z
 * graczy ma swój kolor talii. Program tworzy 4 talie oraz wyświetla stan
 * początkowy gry.
 * 
 * @author Agata Migalska
 *
 */
public class Gra {

	private Talia taliaPomaranczowa;
	private Talia taliaZielona;
	private Talia taliaNiebieska;
	private Talia taliaFioletowa;

	public Gra() {
		taliaFioletowa = new Talia();
		taliaZielona = new Talia();
		taliaNiebieska = new Talia();
		taliaPomaranczowa = new Talia();
	}

	public static void main(String[] args) {
		Gra gra = new Gra();
		LigrettoGUI gui = new LigrettoGUI(gra);
		gui.stworzGui();

	}

	public Talia getTaliaPomaranczowa() {
		return taliaPomaranczowa;
	}

	public void setTaliaPomaranczowa(Talia taliaPomaranczowa) {
		this.taliaPomaranczowa = taliaPomaranczowa;
	}

	public Talia getTaliaZielona() {
		return taliaZielona;
	}

	public void setTaliaZielona(Talia taliaZielona) {
		this.taliaZielona = taliaZielona;
	}

	public Talia getTaliaNiebieska() {
		return taliaNiebieska;
	}

	public void setTaliaNiebieska(Talia taliaNiebieska) {
		this.taliaNiebieska = taliaNiebieska;
	}

	public Talia getTaliaFioletowa() {
		return taliaFioletowa;
	}

	public void setTaliaFioletowa(Talia taliaFioletowa) {
		this.taliaFioletowa = taliaFioletowa;
	}

}
