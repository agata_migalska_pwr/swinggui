package pl.edu.pwr.ligretto.model;

import java.awt.Color;
import java.util.Collections;
import java.util.Stack;

/**
 * Klasa opisująca talię kart. Talia zawiera 40 kart, po 10 kart w każdym z 4
 * kolorów. Do implementacji talii wykorzystano stos.
 * 
 * @author Agata Migalska
 *
 */
public class Talia {

	private static final int LICZBA_KART = 10;
	private static final Color[] KOLORY = new Color[] { Color.BLUE,
			Color.GREEN, Color.ORANGE, Color.MAGENTA };
	private Stack<Karta> karty;

	public Talia() {
		karty = new Stack<Karta>();
		for (int j = 0; j < KOLORY.length; j++) {
			for (int i = 1; i <= LICZBA_KART; i++) {
				karty.add(new Karta(i, KOLORY[j]));
			}
		}
		potasuj();
	}

	public Stack<Karta> getKarty() {
		return karty;
	}

	public void potasuj() {
		Collections.shuffle(karty);
	}

}
