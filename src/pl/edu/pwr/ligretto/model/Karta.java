package pl.edu.pwr.ligretto.model;

import java.awt.Color;

/**
 * Klasa opisująca kartę. Karta ma kolor i numer. Wykorzystano kolor z
 * biblioteki awt, który jest też wykorzystywany przez komponenty biblioteki
 * Swing.
 * 
 * @author Agata Migalska
 *
 */
public class Karta {

	private int numer;
	private Color kolor;

	public Karta(int numer, Color kolor) {
		super();
		this.numer = numer;
		this.kolor = kolor;
	}

	@Override
	public String toString() {
		return "" + numer;
	}

	public Color getKolor() {
		return kolor;
	}

	public void setKolor(Color kolor) {
		this.kolor = kolor;
	}

}
