package pl.edu.pwr.ligretto.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import pl.edu.pwr.ligretto.Gra;

public class LigrettoGUI {

	private Gra gra;

	public LigrettoGUI(Gra gra) {
		this.gra = gra;
	}

	public void stworzGui() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
		frame.setTitle("Ligretto");
		
		PanelGracza panelPomaranczowy = new PanelGracza(Color.ORANGE, gra.getTaliaPomaranczowa());
		panelPomaranczowy.setLayout(new BoxLayout(panelPomaranczowy, BoxLayout.X_AXIS));
		PanelGracza panelNiebieski = new PanelGracza(Color.BLUE, gra.getTaliaNiebieska());
		panelNiebieski.setLayout(new BoxLayout(panelNiebieski, BoxLayout.X_AXIS));
		PanelGracza panelZielony = new PanelGracza(Color.GREEN, gra.getTaliaZielona());
		panelZielony.setLayout(new BoxLayout(panelZielony, BoxLayout.Y_AXIS));
		PanelGracza panelFioletowy = new PanelGracza(Color.MAGENTA, gra.getTaliaFioletowa());
		panelFioletowy.setLayout(new BoxLayout(panelFioletowy, BoxLayout.Y_AXIS));
		
		frame.getContentPane().add(BorderLayout.SOUTH, panelPomaranczowy);
		frame.getContentPane().add(BorderLayout.NORTH, panelNiebieski);
		frame.getContentPane().add(BorderLayout.EAST, panelZielony);
		frame.getContentPane().add(BorderLayout.WEST, panelFioletowy);

		Stol stol = new Stol();
		frame.getContentPane().add(BorderLayout.CENTER, stol);

		frame.setVisible(true);
	}

}
