package pl.edu.pwr.ligretto.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import pl.edu.pwr.ligretto.model.Karta;
import pl.edu.pwr.ligretto.model.Talia;

/**
 * Panel gracza to element wizualny okna, w którym zaprezentowane są karty w
 * rzędzie gracza oraz pierwsza karta na stosie.
 * 
 * PanelGracza implementuje interfejs {@link ActionListener} i w zamyśle obsługuje
 * zdarzenia związane z tym panelem.
 * 
 * @author Agata Migalska
 *
 */
public class PanelGracza extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2130920653556661891L;
	private Talia talia;

	public PanelGracza(Color kolor, Talia talia) {
		this.talia = talia;
		for (int i = 0; i < 3; i++) {
			Karta karta = talia.getKarty().pop();
			JButton rzad = new JButton(karta.toString());
			rzad.setBackground(karta.getKolor());
			rzad.setForeground(Color.WHITE);
			rzad.setSize(100, 100);
			rzad.addActionListener(this);
			this.add(rzad);
		}
		this.add(Box.createRigidArea(new Dimension(10, 10)));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		Karta karta = talia.getKarty().get(3);
		JButton stos = new JButton(karta.toString());
		stos.setBackground(karta.getKolor());
		stos.setForeground(Color.WHITE);
		stos.setSize(100, 100);
		stos.addActionListener(this);
		this.add(stos);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if (!talia.getKarty().isEmpty()) {
			Karta karta = talia.getKarty().pop();
			button.setBackground(karta.getKolor());
			button.setText(karta.toString());
		} else {
			button.setBackground(Color.GRAY);
			button.setText("");
			button.setEnabled(false);
		}
		button.repaint();
	}

}
