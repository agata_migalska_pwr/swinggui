package pl.edu.pwr.ligretto.gui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Klasa opisująca stół / środek w grze w Ligretto. Gdy w grze uczestniczy 4
 * graczy, na środku stołu może powstać do 16 kupek z kartami.
 * 
 * @author Agata Migalska
 *
 */
public class Stol extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 570461579165389918L;

	public Stol() {
		narysujStol();
	}

	private void narysujStol() {
		this.setLayout(new GridLayout(4, 4));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				JButton kupka = new JButton();
				kupka.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
				this.add(kupka);
			}
		}		
	}

}
